using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioClip music;
    [SerializeField] private AudioClip buttonSfx;
    [SerializeField] private AudioClip hitSfx;
    [SerializeField] private AudioClip coinSfx;

    private AudioSource _audioSource;

    private static AudioManager _instance;
    public static AudioManager Instance => _instance;

    private bool _alreadyPlayingMusic;

    private void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        DontDestroyOnLoad(gameObject);

        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = music;

        SceneManager.sceneLoaded += (scene, mode) =>
        {
            if (scene.buildIndex == 1)
            {
                PlayMusic();
            }
            if (scene.buildIndex == 0)
            {
                StopMusic();
            }
        };
    }

    public void PlayMusic()
    {
        if (_alreadyPlayingMusic) return;

        _audioSource.Play();
        _alreadyPlayingMusic = true;
    }

    public void StopMusic()
    {
        _audioSource.Stop();
        _alreadyPlayingMusic = false;
    }

    public void PlayButtonSfx()
    {
        _audioSource.PlayOneShot(buttonSfx);
    }
    
    public void PlayHitSfx()
    {
        _audioSource.PlayOneShot(hitSfx);
    }
    
    public void PlayCoinSfx()
    {
        _audioSource.PlayOneShot(coinSfx);
    }
}