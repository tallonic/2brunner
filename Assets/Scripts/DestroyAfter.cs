using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    [SerializeField] private float time;

    private float _initialTime;
    
    void Start()
    {
        _initialTime = Time.time;
    }

    private void Update()
    {
        if (Time.time > _initialTime + time)
        {
            Destroy(gameObject);
        }
    }
}