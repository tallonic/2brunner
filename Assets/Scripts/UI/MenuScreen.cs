using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class MenuScreen : MonoBehaviour
{
    [SerializeField] private Canvas menuCanvas;
    
    private Canvas _currentCanvas;

    private void Start()
    {
        _currentCanvas = GetComponent<Canvas>();
    }

    public void Show()
    {
        menuCanvas.enabled = false;
        _currentCanvas.enabled = true;
    }
    
    public void Hide()
    {
        menuCanvas.enabled = true;
        _currentCanvas.enabled = false;
    }
}
