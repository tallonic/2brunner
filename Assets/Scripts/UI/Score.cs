using System;
using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI distanceText;
    
    private static Score _instance;
    public static Score Instance => _instance;

    public int ScoreData => Convert.ToInt32(scoreText.text.Replace("Points: ", ""));
    public int DistanceData => Convert.ToInt32(distanceText.text.Replace("Distance: ", ""));
    
    void Start()
    {
        _instance = this;
    }

    public void SetDistance(float distance)
    {
        distanceText.text = "Distance: " + (int)distance;
    }

    public void IncreaseScore()
    {
        int score = ScoreData;
        score++;
        scoreText.text = "Points: " + score;
    }
}
