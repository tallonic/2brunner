using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Fanfare : MonoBehaviour
{
    [SerializeField] private Image fade;

    [Header("Score References")] [SerializeField]
    private Canvas scoreCanvas;
    [SerializeField] private TextMeshProUGUI currentScorePoints;
    [SerializeField] private TextMeshProUGUI currentScoreDistance;
    [SerializeField] private TextMeshProUGUI bestScorePoints;
    [SerializeField] private TextMeshProUGUI bestScoreDistance;

    private static Fanfare _instance;
    public static Fanfare Instance => _instance;
    
    void Start()
    {
        _instance = this;
    }
    
    public void Show()
    {
        StartCoroutine(ShowRoutine());
    }

    private IEnumerator ShowRoutine()
    {
        SetScoreData();

        yield return FadeBackground();

        scoreCanvas.enabled = true;
    }

    IEnumerator FadeBackground()
    {
        Color startColor = fade.color;
        Color endColor = new Color(startColor.r, startColor.g, startColor.b, 0.4f);
        float startTime = Time.time;

        while (Time.time < startTime + 1f)
        {
            float t = (Time.time - startTime) / 1f;
            fade.color = Color.Lerp(startColor, endColor, t);

            yield return null;
        }

        fade.color = endColor;
    }

    private void SetScoreData()
    {
        var currentPoints = Score.Instance.ScoreData;
        var currentDistance = Score.Instance.DistanceData;
        var bestPoints = PlayerPrefs.GetInt("bestPoints");
        var bestDistance = PlayerPrefs.GetInt("bestDistance");

        if (currentPoints > bestPoints)
        {
            PlayerPrefs.SetInt("bestPoints", currentPoints);
            bestPoints = currentPoints;
        }

        if (currentDistance > bestDistance)
        {
            PlayerPrefs.SetInt("bestDistance", currentDistance);
            bestDistance = currentDistance;
        }

        currentScorePoints.text = currentPoints.ToString();
        currentScoreDistance.text = currentDistance.ToString();
        bestScorePoints.text = bestPoints.ToString();
        bestScoreDistance.text = bestDistance.ToString();
    }
}