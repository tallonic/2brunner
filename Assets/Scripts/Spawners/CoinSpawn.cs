using UnityEngine;

public class CoinSpawn : MonoBehaviour
{
    [SerializeField] private GameObject coinPrefab;
    [SerializeField] private int maxCoinSpawn;
    
    public void SpawnCoins(float initialSafeZone, float finalSafeZone)
    {
        var maxSpace = finalSafeZone - initialSafeZone;
        var maxAmountOfCoins = (int)Mathf.Floor(maxSpace) / 10;
        maxAmountOfCoins = Mathf.Max(maxAmountOfCoins, maxCoinSpawn);

        var numberOfCoins = Random.Range(0, maxAmountOfCoins);

        var spawnPosition = finalSafeZone - 15;
        for (int i = 0; i < numberOfCoins; i++)
        {
            var coin = Instantiate(coinPrefab, transform);
            coin.transform.localPosition = new Vector3(spawnPosition, 0f);

            spawnPosition -= 10;

            if (spawnPosition < initialSafeZone + 15)
                break;
        }
    }
}
