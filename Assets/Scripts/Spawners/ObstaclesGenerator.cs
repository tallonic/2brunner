using System.Collections;
using UnityEngine;

public class ObstaclesGenerator : MonoBehaviour
{
    [SerializeField] private GameObject obstaclePrefab;
    [SerializeField] private bool useAlternative;
    
    [Header("Camera offset")] [SerializeField]
    private float spawnCamOffset = 70f;

    [Header("Obstacle Size")] [SerializeField]
    private float minObstacleSize = 1f;

    [SerializeField] private float maxObstacleSize = 1.5f;

    [Header("ObstacleDangerZone")] [SerializeField]
    private float minObstacleDangerZone = 20;

    [SerializeField] private float maxObstacleDangerZone = 30;

    [Header("SafeZone")] [SerializeField] private float minSafeZone = 20;
    [SerializeField] private float maxSafeZone = 30;

    [Header("References")]
    [SerializeField] private CoinSpawn coinSpawn;

    private Camera _cam;

    void Start()
    {
        _cam = Camera.main;
        StartCoroutine(SpawnObstacles());
    }

    private IEnumerator SpawnObstacles()
    {
        var nextObstacle = GenerateNewObstacle();
        nextObstacle.transform.localPosition = new Vector2(_cam.transform.position.x + spawnCamOffset, 0f);
        nextObstacle.SetActive(true);

        while (true)
        {
            var nextObstacleSpawnPoint = nextObstacle.transform.position;
            
            var dangerZone = GetObjectDangerZone(nextObstacle);
            var safeZone = GetSafeZone();

            nextObstacle = GenerateNewObstacle();
            var nextDangerZone = GetObjectDangerZone(nextObstacle);
            
            nextObstacleSpawnPoint.x += dangerZone;

            var initialSafeZone = nextObstacleSpawnPoint;
            nextObstacleSpawnPoint.x += safeZone;
            var endSafeZone = nextObstacleSpawnPoint;
            
            nextObstacleSpawnPoint.x += nextDangerZone;

            coinSpawn.SpawnCoins(initialSafeZone.x, endSafeZone.x);
            
            yield return new WaitWhile(() => _cam.transform.position.x + spawnCamOffset < nextObstacleSpawnPoint.x);
            
            nextObstacle.transform.position = nextObstacleSpawnPoint;
            nextObstacle.SetActive(true);
            
            
        }
    }

    private GameObject GenerateNewObstacle()
    {
        var newGameObject = Instantiate(obstaclePrefab, transform, true);
        var alternativeSkin = newGameObject.GetComponent<AlternativeSkin>();
        if (alternativeSkin != null)
            alternativeSkin.SetAlternativeSkin(useAlternative);
        
        var scale = Random.Range(minObstacleSize, maxObstacleSize);
        newGameObject.transform.localScale = new Vector3(scale, scale, scale);

        newGameObject.SetActive(false);

        return newGameObject;
    }

    private float GetSafeZone()
    {
        return Random.Range(minSafeZone, maxSafeZone);
    }

    private float GetObjectDangerZone(GameObject lastObstacle)
    {
        if (lastObstacle == null) return 0f;

        var obstacleSizeDiff = maxObstacleSize - minObstacleSize;
        var obstacleDangerZoneDiff = maxObstacleDangerZone - minObstacleDangerZone;
        var lastObstacleSizeDiff = lastObstacle.transform.localScale.y - minObstacleSize;

        return (obstacleDangerZoneDiff * lastObstacleSizeDiff) / obstacleSizeDiff;
    }
}