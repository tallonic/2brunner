using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ParallaxMovement : MonoBehaviour
{
    [SerializeField] private float speedX = 0f;

    private void Start()
    {
        var rb2D = GetComponent<Rigidbody2D>();
        rb2D.velocity = new Vector2(speedX, 0f);
    }
}
