using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private GameObject backgroundPrefab;
    [SerializeField] private float cameraOffset = 96f;
    [SerializeField] private float backgroundCreationOffset = 64f;
    [SerializeField] private bool useAlternative;

    private Camera _cam;

    private List<GameObject> _backgrounds = new();

    private void Start()
    {
        _cam = Camera.main;

        foreach (Transform child in transform)
        {
            _backgrounds.Add(child.gameObject);
        }
    }

    private void Update()
    {
        if (_cam.transform.position.x > _backgrounds[0].transform.position.x + cameraOffset)
        {
            DestroyPreviousBackground();
            CreateNewBackground();
        }
    }

    private void CreateNewBackground()
    {
        var lastBackgroundPosition = _backgrounds.Last().transform.position;
        var newXPosition = lastBackgroundPosition.x + backgroundCreationOffset;
        var newPosition = new Vector3(newXPosition, lastBackgroundPosition.y);
        var background = Instantiate(backgroundPrefab, newPosition, quaternion.identity);
        var alternativeSkin = background.GetComponent<AlternativeSkin>();
        if (alternativeSkin != null)
            alternativeSkin.SetAlternativeSkin(useAlternative);
        background.transform.SetParent(transform);
        background.transform.rotation = new Quaternion(0f, 0f, 0f, 0f);

        _backgrounds.Add(background);
    }

    private void DestroyPreviousBackground()
    {
        Destroy(_backgrounds[0]);
        _backgrounds.RemoveAt(0);
    }
}