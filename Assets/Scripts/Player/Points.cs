using UnityEngine;

public class Points : MonoBehaviour
{
    public int _points { get; private set; }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Coin"))
        {
            Destroy(other.gameObject);
            AudioManager.Instance.PlayCoinSfx();
            _points++;
            
            Score.Instance.IncreaseScore();
        }
    }
}
