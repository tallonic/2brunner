using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float initialSpeed;

    public float CurrentSpeed { get; private set; }
    
    void Start()
    {
        CurrentSpeed = initialSpeed;
        GetComponent<Rigidbody2D>().velocity = new Vector2(CurrentSpeed, 0f);
    }

    private void FixedUpdate()
    {
        Score.Instance.SetDistance((transform.position.x + 15) / 10);
    }
}