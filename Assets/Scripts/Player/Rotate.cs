using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D))]
public class Rotate : MonoBehaviour
{
    [SerializeField] private CharacterMovement characterMovement;
    [SerializeField] private float rotateVariation = .1f;
    [SerializeField] private InputType inputType;
    [SerializeField] private bool inverseControl;

    [Header("OtherCharacterReference")] 
    [SerializeField] private Rigidbody2D otherCharacterRb2D;

    private Rigidbody2D _rb2D;
    private float _currentRotation;
    private float _elapsedTime;
    private bool? _isRotatingToUp;

    private KeyCode _upKeyCode;
    private KeyCode _downKeyCode;
    
    public bool IsDead { get; set; }
    
    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();

        if (inputType == InputType.Arrows)
        {
            _upKeyCode = inverseControl ? KeyCode.DownArrow : KeyCode.UpArrow;
            _downKeyCode = inverseControl ? KeyCode.UpArrow : KeyCode.DownArrow;
        }
        else
        {
            _upKeyCode = inverseControl ? KeyCode.S : KeyCode.W;
            _downKeyCode = inverseControl ? KeyCode.W : KeyCode.S;
        }
        
    }

    void Update()
    {
        if (IsDead) return;
        
        if (Input.GetKey(_upKeyCode))
        {
            if (_isRotatingToUp == null || !_isRotatingToUp.Value)
            {
                _elapsedTime = 0f;
                _isRotatingToUp = true;
            }

            RotateUp();
        }
        else if (Input.GetKey(_downKeyCode))
        {
            if (_isRotatingToUp == null || _isRotatingToUp.Value)
            {
                _elapsedTime = 0f;
                _isRotatingToUp = false;
            }

            RotateDown();
        }
        else
        {
            _elapsedTime = 0f;
        }
    }

    public void RotateUp()
    {
        _currentRotation = Mathf.LerpAngle(_currentRotation, characterMovement.CurrentSpeed, _elapsedTime);
        _elapsedTime += Time.deltaTime * rotateVariation;

        var direction = new Vector2(characterMovement.CurrentSpeed, _currentRotation).normalized;
        _rb2D.velocity = direction * characterMovement.CurrentSpeed;

        NormalizeOtherCharacterSpeed();
    }

    private void NormalizeOtherCharacterSpeed()
    {
        var otherCharacterVectorMovement = otherCharacterRb2D.velocity;
        otherCharacterVectorMovement.x = _rb2D.velocity.x;
        otherCharacterRb2D.velocity = otherCharacterVectorMovement;
    }

    public void RotateDown()
    {
        _currentRotation = Mathf.LerpAngle(_currentRotation, -characterMovement.CurrentSpeed, _elapsedTime);
        _elapsedTime += Time.deltaTime * rotateVariation;

        var direction = new Vector2(characterMovement.CurrentSpeed, _currentRotation).normalized;
        _rb2D.velocity = direction * characterMovement.CurrentSpeed;
        
        NormalizeOtherCharacterSpeed();
    }
}

public enum InputType
{
    Arrows,
    WASD
}