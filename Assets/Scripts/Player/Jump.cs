using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Jump : MonoBehaviour
{
    [SerializeField] private float jumpForce;
    [SerializeField] private float positivePenalty;
    
    private Rigidbody2D _rb2D;
    
    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();
    }
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            var force = _rb2D.velocity.y < 0f ? jumpForce : jumpForce / positivePenalty;
            _rb2D.AddForce(new Vector2(0f, force));
        }
    }
}
