using Unity.VisualScripting;
using UnityEngine;


public class DeathCollision : MonoBehaviour
{
    [SerializeField] private Rotate rotate;
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        Death();
    }

    private void Death()
    {
        rotate.IsDead = true;
        
        AudioManager.Instance.PlayHitSfx();
        
        var characters = FindObjectsByType<CharacterMovement>(FindObjectsInactive.Exclude, FindObjectsSortMode.None);
        for (int i = 0; i < characters.Length; i++)
        {
            characters[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }

        var backgrounds = FindObjectsByType<ParallaxMovement>(FindObjectsInactive.Exclude, FindObjectsSortMode.None);
        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        
        Fanfare.Instance.Show();
    }
}
