using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class SpriteUpdater : MonoBehaviour
{
    [SerializeField] private Transform spriteTransform;
    private Rigidbody2D _rb2D;

    private void Start()
    {
        _rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        var dir = _rb2D.velocity;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        spriteTransform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
}
