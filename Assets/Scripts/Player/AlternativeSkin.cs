using System.Collections;
using UnityEngine;

public class AlternativeSkin : MonoBehaviour
{
    [SerializeField] private bool useAlternativeSprite;
    [SerializeField] private Sprite altSprite;

    private void Start()
    {
        if (useAlternativeSprite)
        {
            GetComponent<SpriteRenderer>().sprite = altSprite;

            ChangeAnimatorSkin();
        }
    }

    public void SetAlternativeSkin(bool useAlternative)
    {
        useAlternativeSprite = useAlternative;
        ChangeAnimatorSkin();
    }

    private void ChangeAnimatorSkin()
    {
        if (TryGetComponent(out Animator animator))
        {
            animator.SetTrigger("SetAlternativeSkin");
        }
    }
}